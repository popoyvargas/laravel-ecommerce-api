<?php

namespace App\Exceptions;

use Exception;

class UserDoesNotOwnProductException extends Exception
{
	public function render()
	{
		return [
			'errors' => 'User has no ownership to the product!'
		];
	}
}